// +----------------------------------------------------------------------
// |万岳科技开源系统 [山东万岳信息科技有限公司]
// +----------------------------------------------------------------------
// | Copyright (c) 2020~2022 https://www.sdwanyue.com All rights reserved.
// +----------------------------------------------------------------------
// | 万岳科技相关开源系统，需标注"代码来源于万岳科技开源项目"后即可免费自用运营，前端运营时展示的内容不得使用万岳科技相关信息
// +----------------------------------------------------------------------
// | Author: 万岳科技开源官方 < wanyuekj2020@163.com >
// +----------------------------------------------------------------------
#import "OptimizationCell.h"
#import "SubmitOrderViewController.h"
@implementation OptimizationCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (IBAction)shareBuyButtonClick:(id)sender {
    [MBProgressHUD showMessage:@""];
    //new 0:加入购物车 1:立即购买的时候加入购物车
    NSDictionary *dic = @{
        @"productId":_model.goodsID,
        @"cartNum": @"1",
        @"uniqueId":@"",
        @"new":@"1"
    };
    [WYToolClass postNetworkWithUrl:@"cart/add" andParameter:dic success:^(int code, id  _Nonnull info, NSString * _Nonnull msg) {
        if (code == 200) {
            [self doBuyAndPayView:minstr([info valueForKey:@"cartId"])];
        }
    } fail:^{
        
    }];

}
- (void)doBuyAndPayView:(NSString *)cartID{
    [WYToolClass postNetworkWithUrl:@"order/confirm" andParameter:@{@"cartId":cartID} success:^(int code, id  _Nonnull info, NSString * _Nonnull msg) {
        if (code == 200) {
            [MBProgressHUD hideHUD];
            SubmitOrderViewController *vc = [[SubmitOrderViewController alloc]init];
            vc.orderMessage = [info mutableCopy];
            vc.liveUid = @"";
            [[MXBADelegate sharedAppDelegate] pushViewController:vc animated:YES];
        }
    } fail:^{
        
    }];

}

- (void)setModel:(optimizationModel *)model{
    _model = model;
    [_thumbImgView sd_setImageWithURL:[NSURL URLWithString:_model.thumb]];
    _nameL.attributedText = [self setTextSpeace:_model.name];
    _priceLabel.text = _model.price;
    _salesL.text = [NSString stringWithFormat:@"%@人已买",_model.sales];
}
- (NSAttributedString *)setTextSpeace:(NSString *)str{
    NSMutableAttributedString *muStr = [[NSMutableAttributedString alloc]initWithString:str];
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    [paragraphStyle setLineSpacing:5];        //设置行间距
    [muStr addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [muStr length])];

    return muStr;
}
@end
